package nl.utwente.di.tempConvert;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class tempConvert extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public double convert(String tempStr) {
        return Double.parseDouble(tempStr) * 1.8 + 32;
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Converter";

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("ctemp") + "\n" +
                "  <P>Temperature converted to Fahrenheit: " +
                convert(request.getParameter("ctemp")) +
                "</BODY></HTML>");
    }
}
